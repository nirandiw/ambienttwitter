package org.ambientdynamix.contextplugins.ambientTwitter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.*;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Nirandika Wanigasekara on 5/3/2015.
 */
public class twitterAuthorization implements IPluginView {

    private String TAG = this.getClass().getSimpleName();
    private Context context;
    private ContextPluginRuntime runtime;
    private String message;
    private ActivityController controller;



    public twitterAuthorization(Context context, ContextPluginRuntime runtime, String message) {
        Log.i(TAG, "twitterAuthorization");
        this.context = context;
        this.runtime = runtime;
        this.message = message;
    }

    @Override
    public void setActivityController(ActivityController activityController) {
        this.controller = activityController;
    }

    /*Creats a web view to display the twitter login page*/
    @Override
    public View getView() { //TODO: Check with Darren if its possible to pass the layout xml file
        Log.i(TAG,"the web view for Twitter login");
        // Discover our screen size for proper formatting
        DisplayMetrics met = context.getResources().getDisplayMetrics();
        Log.i(TAG, "Screen Size (WxH): " + met.widthPixels + "x" + met.heightPixels);

        // Access our Locale via the incoming context's resource configuration to determine language
        String language = context.getResources().getConfiguration().locale.getDisplayLanguage();
        Log.i(TAG, "Languate: " + language);

        // Create a relative layout
        RelativeLayout relativeLayout= new RelativeLayout(context); //TODO: Did not set the orientation.
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));

        //Create a web view
        WebView webView=new WebView(context);
        if (message!=null){
            webView.loadUrl(message);
        }else{
            Log.i(TAG,"Authorized URL form Twitter is null");
            destroyView();
        }

        webView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        relativeLayout.addView(webView);

        //TODO: Find a way to store the access Tokens and close the ipluginview.
        return relativeLayout;



    }

    @Override
    public void destroyView() {
        controller.closeActivity();
    }

    @Override
    public void handleIntent(Intent intent) { //TODO: Think of a way to use this

    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}
